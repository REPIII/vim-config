"""""""""""""""
" BASIC SETUP "
"""""""""""""""
set number " Show line number
" Line numbers will be relative to where the cursor is
set relativenumber relativenumber 
" Vim will not try to be compatable with vi
set nocompatible
" Hides buffers instead of closing them
set hidden
" Ignore case when searching
set ignorecase
" Ignore case if search pattern is all lowercase IF "set ignorecase"
" is set
set smartcase 
" lines will wrap after 79 characters
set textwidth=79
" Line wrap appropriate for 11" Chromebook
" set textwidth=68
" Highlight search terms
set hlsearch
" Unhighlight search terms with ctr-L
nnoremap <C-l> :let @/ = ''<CR>
" 'set list' will show these whitespace characters
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<

" Shortcut to enable spellchecker locally
command SP setlocal spell spelllang=en_us
" And to turn it off
command SPN set nospell

" Shortcut to make whitespace visible. It toggles.
command L set list!

" Uppercase aliases for frequently used commands.
" Shfting to press ':' means uppercase commands are common.
command WQA wqa 
command WQa wqa
command Wqa wqa
command WQ wq
command Wq wq
command W w
command Q q
cmap Q! q!

" Allows use of w!! to edit file that required root after opening
" not as root
cmap w!! w !sudo tee % >/dev/null

" TAB CONTROLS
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set smarttab

" THEME
syntax enable
set background=dark
set t_Co=256
let g:solarized_termcolors=256
let g:solarized_contrast="high"
colorscheme solarized

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" The rest is based on https://www.youtube.com/watch?v=XA2WjJbmmoM "
" https://github.com/mcantor/no_plugins/blob/master/no_plugins.vim "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Finding files "
filetype plugin on
set path+=**
set wildmenu

" TAG JUMPING "
" ^] " This is commented out because VIM does not consider this an
" editor command 2018/01/19
command! MakeTags !ctags -R .

" FILE BROWSING "
let g:netrw_banner=0        " disable annoying banner
let g:netrw_browse_split=4  " open in prior window
let g:netrw_altv=1          " open splits to the right
let g:netrw_liststyle=3     " tree view
" This hides all dirs/files for some reason
" let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide=',\(^\|\s\s\)\zs\.\S\+'

" Vim sets its current directory to the directory it's opened from
set autochdir


" SNIPPETS "
nnoremap ,c :-1read $HOME/.vim/snippets/skeleton.c<CR>7j4l
nnoremap ,cpp :-1read $HOME/.vim/snippets/skeleton.cpp<CR>7j4l
nnoremap ,go :-1read $HOME/.vim/snippets/skeleton.go<CR>6j14l
nnoremap ,html :-1read $HOME/.vim/snippets/skeleton.html<CR>6j8l
nnoremap ,py :-1read $HOME/.vim/snippets/skeleton.py<CR>10j
nnoremap ,sh :-1read $HOME/.vim/snippets/skeleton.sh<CR>4j4l
