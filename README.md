vim-config
==========

My simple Vim setup; online and public for easy access when my
hard drive inevitably dies.

I do not pretend to be deeply knowledgeable about Vim. If
you're looking to start your own custom vim setup, do not start here.
There are better places to get started.

This setup purposefully avoids the use of plugins.

Feel free to submit merge requests with improvements, particularly
basic alterations. I'm always happy to learn more about Vim.

-------

Constructed for VIM 8.2 running on a Debian family system.

This repository is under a Unlicense. See license.txt for more
details.
